{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE InstanceSigs #-}

{- |
Module      :  Data.Exponents
Description :  <optional short text displayed on contents page>
Copyright   :  (c) Ossi Herrala
License     :  MIT

Maintainer  :  Ossi Herrala <oherrala@gmail.com>
Stability   :  experimental
Portability :  portable

<module description starting at first column>
-}

module Data.Exponents where

import Data.List (sortBy, group)
import Data.Numbers.Primes (primes, primeFactors)

data Exponents a = Exp a a            -- ^ Exponentation (base ^ power)
                 | Sum [Exponents a]  -- ^ Sum of Exponents
                 | Prod [Exponents a] -- ^ Product of Exponents
                 deriving Show


base :: Integral a => Exponents a -> a
base x = case reduce x of
  (Exp b _) -> b

power :: Integral a => Exponents a -> a
power x = case reduce x of
  (Exp _ p) -> p


instance Integral a => Eq (Exponents a) where
  x == y = compare x y == EQ

instance (Integral a, Ord a) => Ord (Exponents a) where
  x@(Exp _ _) `compare` y@(Exp _ _) = let (Exp b1 p1) = reduce x
                                          (Exp b2 p2) = reduce y
                                      in case b1 `compare` b2 of
                                        EQ  -> p1 `compare` p2
                                        ord -> ord


instance Integral a => Num (Exponents a) where
  x + y                     = reduce $ Sum [x, y]
  x - y                     = reduce $ Sum [x, negate y]

  x@(Exp b1 p1) * y@(Exp b2 p2)
    | b1 == b2 = factorize $ Exp b1 (p1+p2)
    | otherwise = reduce $ Prod [factorize x, factorize y]

  x * y                     = reduce $ Prod [factorize x, factorize y]

  negate a                  = reduce $ Exp (-1) 1 * a

  signum (Exp b p)
    | b == 0 && p == 0      = 1
    | b == 0 && p > 0       = 0
    | b == 0 && p < 0       = error "Negative exponent"
    | b > 0                 = 1
    | b < 0 && even p       = 1
    | b < 0 && odd p        = -1

  signum (Prod xs)          = product . map signum $ xs

  abs x@(Exp b p)
    | b == 0 && p == 0      = 1
    | b == 0 && p > 0       = 0
    | b == 0 && p < 0       = error "Negative exponent"
    | b < 0 && even p       = reduce $ Exp (abs b) p
    | b < 0 && odd p        = reduce x
    | otherwise             = reduce x

  fromInteger n             = reduce . factorize $ Exp (fromIntegral n) 1


-- ^ Reduce and normalize given value as much as possible.
reduce :: Integral a => Exponents a -> Exponents a

-- |
-- If the exponent is positive, the power of zero is zero: 0^n = 0,
-- where n > 0.
-- If the exponent is negative, the power of zero (0^n, where n < 0) is
-- undefined, because division by zero is implied.
--   Ref. https://en.wikipedia.org/wiki/Exponentiation#Powers_of_zero
--   Ref. https://en.wikipedia.org/wiki/Exponentiation#Zero_to_the_power_of_zero
--
-- >>> reduce $ Exp 0 5
-- Exp 0 1
reduce (Exp 0 a)
  | a > 1  = Exp 0 1
  | a < 0  = error "Division by zero"

-- |
-- If the exponent is zero, some authors define 0^0 = 1, whereas others
-- leave it undefined.
--   Ref. https://en.wikipedia.org/wiki/Exponentiation#Powers_of_zero
--   Ref. https://en.wikipedia.org/wiki/Exponentiation#Treatment_on_computers
--
-- >>> reduce $ Exp 5 0
-- Exp 1 1
reduce (Exp _ 0) = Exp 1 1

-- |
-- The integer powers of one are all one: 1^n = 1.
--   Ref. https://en.wikipedia.org/wiki/Exponentiation#Powers_of_one
--
-- >>> reduce $ Exp 1 5
-- Exp 1 1
reduce (Exp 1 _) = Exp 1 1

-- |
-- If n is an even integer, then (−1)^n = 1.
-- If n is an odd integer, then (−1)^n = −1.
--   Ref. https://en.wikipedia.org/wiki/Exponentiation#Powers_of_minus_one
--
-- >>> reduce $ Exp (-1) 2
-- Exp 1 1
-- >>> reduce $ Exp (-1) 3
-- Exp (-1) 1
reduce (Exp (-1) n)
  | even n = Exp 1 1
  | odd n  = Exp (-1) 1

reduce (Sum [x])  = reduce x
reduce (Sum xs)   = Sum . reduceSum . sortExponents $ xs

reduce (Prod [x]) = reduce x
reduce (Prod xs)  = Prod . reduceProd . sortExponents $ xs

-- Catch rest
reduce a = a


reduceSum :: Integral a => [Exponents a] -> [Exponents a]
reduceSum []                  = []
reduceSum (Sum x : xs)        = reduceSum . sortExponents $ x ++ xs
reduceSum (Exp 0 1 : xs)      = reduceSum xs
reduceSum (x@(Exp _ _) : xs) = let
  len   = fromIntegral . length . filter (==x) $ xs
  newX  = if len >= 1 then Prod [Exp (len+1) 1, x] else x
  newXS = filter (/=x) $ xs
  in newX : reduceSum newXS
reduceSum (x : xs)            = reduce x : reduceSum xs

reduceProd :: Integral a => [Exponents a] -> [Exponents a]
reduceProd []                 = []
reduceProd (Prod x : xs)      = reduceProd . sortExponents $ x ++ xs
reduceProd (Exp 0 1 : _)      = [Exp 0 1]
reduceProd (Exp _ 0 : xs)     = reduceProd xs
reduceProd (Exp 1 _ : xs)     = reduceProd xs
reduceProd (x@(Exp _ _) : xs) = let
  newX  = foldr (*) x $ filter ((==base x) . base) xs
  newXS = filter ((/=base x) . base) xs
  in newX : reduceProd newXS
reduceProd (x : xs)           = reduce x : reduceProd xs


sortExponents :: Integral a => [Exponents a] -> [Exponents a]
sortExponents = sortBy sort'
  where
    -- Prefer Prod first, Sum then and Exp last
    sort' :: Integral a => Exponents a -> Exponents a -> Ordering
    sort' (Prod _) (Sum _)        = LT
    sort' (Prod _) (Exp _ _)      = LT
    sort' (Sum _) (Exp _ _)       = LT
    sort' x@(Exp _ _) y@(Exp _ _) = compare x y
    sort' _ _                     = GT


-- | Prime factorize the value and return product of exponents
--
-- >>> factorize 5
-- Exp 5 1
--
-- >>> factorize 50
-- Prod [Exp 2 1,Exp 5 2]
factorize :: Integral a => Exponents a -> Exponents a
factorize x@(Exp b1 p1)
  | b1 == 0 = reduce x
  | b1 == 1 = reduce x
  | p1 == 0 = reduce x

factorize (Exp b1 p1) = reduce . Prod . map (\x -> Exp (head x) (len x * p1)) . group . primeFactors $ b1
  where
    len = fromIntegral . length

-- Catch rest
factorize a = a


-- | Convert exponents into integral value
--
-- >>> fromExponents (Exp 2 2)
-- 4
-- >>> fromExponents (Prod [Exp 3 9, Exp 2 5])
-- 629856
fromExponents :: Integral a => Exponents a -> a
fromExponents (Exp b1 p1) = b1^p1
fromExponents (Sum xs)    = sum . map fromExponents $ xs
fromExponents (Prod xs)   = product . map fromExponents $ xs



-- | Calculate factorial using Legendre's Theorem
-- http://www.cut-the-knot.org/blue/LegendresTheorem.shtml
factorial :: Integral a => a -> Exponents a
factorial n = Prod . sortExponents . map (\p -> Exp p (dp p n)) . takeWhile (<=n) $ primes
  where
    dp :: Integral a => a -> a -> a
    dp p n' = sum . map (\k -> floor $ fromIntegral n'/fromIntegral p^k) $ [1..log' p n']
    log' :: Integral a => a -> a -> a
    log' b n' = floor $ logBase (fromIntegral b) (fromIntegral n')
