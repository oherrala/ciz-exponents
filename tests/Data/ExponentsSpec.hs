
module Data.ExponentsSpec where

import Data.Exponents

import Test.Hspec
import Test.QuickCheck

spec = parallel $ do

  describe "Properties" $ do
    it "fromExponents . fromInteger" $ property $
      \x -> (fromExponents . fromInteger) x == (x :: Integer)

    it "addition: x + y" $ property $
      \(x,y) -> (fromExponents $ fromInteger x + fromInteger y) == (x+y :: Integer)

    it "substraction: x - y" $ property $
      \(x,y) -> (fromExponents $ fromInteger x - fromInteger y) == (x-y :: Integer)

    it "multiplication: x * y" $ property $
      \(x,y) -> (fromExponents $ fromInteger x * fromInteger y) == (x*y :: Integer)

  describe "Comparisons (Eq and Ord)" $ do
    it "0^1 == 0^5" $
      (Exp 0 1 == Exp 0 5) `shouldBe` True

    it "0 /= 1^1" $
      (Exp 0 1 /= Exp 1 1) `shouldBe` True

    it "2^5 /= 0" $
      (Exp 2 5 /= Exp 0 1) `shouldBe` True
      
    it "1^2 /= 2^2" $
      (Exp 1 2 /= Exp 2 2) `shouldBe` True
      
    it "2^2 == 2^2" $
      (Exp 2 2 == Exp 2 2) `shouldBe` True

  describe "factorize" $ do
    it "fromExponents (factorize (Exp 42 42)) == 42^42" $
      fromExponents (factorize (Exp 42 42)) == 42^42 `shouldBe` True
  
  -- describe "Multiplication" $ do
  --   it "2^2 * 2^5 = 2^7" $
  --     (Exp 2 2 * Exp 2 5) `shouldBe` Exp 2 7
  --   it "2^2 * 3^5 = 2^2 * 3^5" $
  --     (Exp 2 2 * Exp 3 5) `shouldBe` Prod [Exp 2 2, Exp 3 5]

  -- describe "Addition" $
  --   it "2^2 + 2^5 = 2^2 + 2^5" $
  --     (Exp 2 2 + Exp 2 5) `shouldBe` Sum [Exp 2 2, Exp 2 5]

  -- describe "Substraction" $
  --   it "2^2 - 2^5 = 2^2 - 2^5" $
  --     (Exp 2 2 - Exp 2 5) `shouldBe` Sum [Exp 2 2, Neg (Exp 2 5)]

  -- describe "Reductions" $ do
  --   it "negate (negate 1^1) = 1" $
  --     reduce (Neg (Neg (Exp 1 1))) `shouldBe` One
  --   it "negate (negate 2^2) = 2^2" $
  --     reduce (Neg (Neg (Exp 2 2))) `shouldBe` Exp 2 2
      
